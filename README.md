
About
=====

The BRAIN project (Brain Replicating Artificial Intelligence Neural-network)
consists of a shared library (libbrain) for doing calculations on neural
networks, a daemon executable (braind) that is intended to run on a server and
do continuos updates of one or more neural networks, and a terminal program
(brainmon) for monitoring and controlling the daemon process.


Status
======

The project is still in its infancy, and does not run in the current state.


Roadmap
=======

The currently planned features are:

libbrain
--------

* Support for several different types of neural networks and learning
algorithms
* SSE Support
* OpenCL Support
* Multithreading support, through work-lists

braind
------

* Continous running of one or more processes with neural networks
* Storing of process statuses on disk (async or lockstep)
* Networking support, running the processes on multiple daemons through
the network
* REST or similar API using JSON to communicate and program the daemon
processes.

brainmon
--------

* Monitor and control the braind process through the REST API

Building
========

It is recommended to build the project out-of-source. Start by creating a
build directory.

```
mkdir build
cd build
```

If you are building from the repository and not from a release tarball,
generate the needed autotools files by running

```
autoreconf -i ..
```

Then run configure and make as follows

```
../configure
make && make install
```

You can also run the included tests for the project by running `make check`
after running `configure`.

For a release build, it is recommended to include the following parameters
to configure

```
../configure --disable-assert CFLAGS='-Os'
```

If you are building for development, some form of the following parameters
are recommended

```
../configure --prefix=/my/dev/root CFLAGS='-O0 -ggdb -Wall -Wextra -pedantic'
```

To run it all in a single command

```
autoreconf -i .. && ../configure CFLAGS='-O0 -ggdb -Wall -Wextra -pedantic'
--prefix=/my/dev/root && make && make check && make install
```


License
=======

This software is provided with the [UNLICENSE](http://unlicense.org) license.

See the included UNLICENSE file or <http://unlicense.org> for more information.
