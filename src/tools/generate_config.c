
#include <stdio.h>
#include <stdlib.h>

#include "../configuration.h"

int main(int argc, char** argv)
{
    if (argc != 2)
    {
        fprintf(stderr, "Usage: braind_generate_config <config_file>\n");
        return EXIT_FAILURE;
    }

    char* fname = argv[1];
    FILE* f = fopen(fname, "w");

    if (f == NULL)
    {
        fprintf(stderr, "Error opening file %s\n", fname);
        return EXIT_FAILURE;
    }

    fprintf(f, "# Braind configuration file\n");

    struct configuration* cfg = default_configuration();

    if (cfg == NULL)
    {
        fprintf(stderr, "Error getting default configuration\n");
        return EXIT_FAILURE;
    }

    struct configuration_option* opt = cfg->options;

    while (opt != NULL)
    {
        fprintf(f, "%s\n", configuration_option_to_string(opt));
        opt = opt->next;
    }

    fclose(f);
    return EXIT_SUCCESS;
}
