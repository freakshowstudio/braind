
#ifndef DAEMON_H
#define DAEMON_H

#include "configuration.h"


int
run_daemon(struct configuration* configuration);


#endif // DAEMON_H
