
#include "autoconf.h"

#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include <stdbool.h>

#include "strings.h"


char*
string_trim_whitespace(char* string)
{
    if (string == NULL)
    {
        return NULL;
    }

    char* start = string;
    while(isspace((char) *start))
    {
        start++;
    }

    if (*start == 0)
    {
        return NULL; // All spaces
    }

    char* end = string + strlen(string) - 1;
    while (end > start && isspace((char) *end))
    {
        end--;
    }

    int n = end - start + 1;
    char* newstring = (char*) malloc(sizeof(char) * n);
    if (newstring == NULL)
    {
        // TODO: Better error handling
        return NULL;
    }
    strncpy(newstring, start, n);
    return newstring;
}
