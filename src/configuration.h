
#ifndef CONFIGURATION_H
#define CONFIGURATION_H

enum configuration_option_type
{
    INTEGER,
    FLOAT,
    STRING,
    IPV4
};


struct configuration_option
{
    char* name;
    enum configuration_option_type type;
    void* value;
    struct configuration_option* next;
};

struct configuration
{
    struct configuration_option* options;
};


struct configuration*
default_configuration();

void
set_configuration_option(
    struct configuration* configuration,
    const char* name,
    const enum configuration_option_type type,
    const void* value);

struct configuration_option*
get_configuration_option(
    struct configuration* configuration,
    const char* name);

char*
configuration_option_to_string(
    struct configuration_option* option);

void
read_configuration_file(struct configuration* cfg, char* path);


#endif // CONFIGURATION_H
