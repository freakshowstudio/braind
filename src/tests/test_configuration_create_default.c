
#include <stdlib.h>
#include "../configuration.h"

int main(int __unused argc, char** __unused argv)
{
    struct configuration* cfg = default_configuration();
    if (cfg == NULL)
    {
        return EXIT_FAILURE;
    }

    return 0;
}
