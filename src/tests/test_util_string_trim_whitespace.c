
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "../util/strings.h"


int main(int __unused argc, char** __unused argv)
{
    char* teststring1 = "test";
    char* teststring2 = " test ";

    char* teststring3 = string_trim_whitespace(teststring2);
    if (teststring3 == NULL)
    {
        fprintf(stderr, "NULL returned\n");
        return EXIT_FAILURE;
    }
    int res1 = strcmp(teststring1, teststring3);
    if (res1 != 0)
    {
        fprintf(stderr, "Non matching strings\n");
        fprintf(stderr,
            "Expected '%s' got '%s'", teststring1, teststring3);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
