
#include "autoconf.h"

#include <stdlib.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <unistd.h>

#include "daemon.h"
#include "configuration.h"

static bool run = true;

static void
sig_handler(int signo)
{
    if (signo == SIGINT)
    {
        fprintf(stdout, "Received SIGINT, terminating\n");
        run = false;
    }
}

int
run_daemon(struct configuration* configuration)
{
    if (signal(SIGINT, sig_handler) == SIG_ERR)
    {
        fprintf(stderr, "Error registering signal handler\n");
        return EXIT_FAILURE;
    }

    while(run)
    {
        sleep(1);
    }

    return EXIT_SUCCESS;
}
