
#include "autoconf.h"

#include <stdbool.h>
#include <stdint.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

#include "configuration.h"
#include "util/strings.h"


#define CONFIGURATION_VALUE_MAXLENGTH 256
#define LINE_BUFFER_LENGTH 1024


static struct configuration_option*
create_configuration_option(
    const char* name,
    const enum configuration_option_type type,
    const void* value)
{
    struct configuration_option* opt =
        malloc(sizeof(struct configuration_option));

    if (opt == NULL)
    {
        fprintf(stderr,
            "Error allocating memory for new configuration option\n");
        return NULL;
    }

    opt->next = NULL;
    opt->name = strdup(name);
    opt->type = type;

    switch (type)
    {
        case INTEGER:
            opt->value = malloc(sizeof(int));
            memcpy(opt->value, value, sizeof(int));
            break;
        case FLOAT:
            opt->value = malloc(sizeof(float));
            memcpy(opt->value, value, sizeof(float));
            break;
        case STRING:
            opt->value = strdup((char*) value);
            break;
        case IPV4:
            opt->value = strdup((char*) value);
            break;
    }

    if (opt->value == NULL)
    {
        fprintf(stderr,
            "Error allocating memory\n");
        return NULL;
    }

    return opt;
}

void
set_configuration_option(
    struct configuration* configuration,
    const char* name,
    const enum configuration_option_type type,
    const void* value)
{
    struct configuration_option* new =
        create_configuration_option(name, type, value);

    if (new == NULL)
    {
        fprintf(stderr,
            "Error creating configuration option\n");
        return;
    }

    struct configuration_option* opt = configuration->options;

    if (opt == NULL)
    {
        configuration->options = new;
        return;
    }

    struct configuration_option* last = NULL;

    while (opt != NULL)
    {
        if (strcmp(name, opt->name) == 0)
        {
            if (last != NULL)
            {
                last->next = new;
            }
            free(opt);
            return;
        }
        last = opt;
        opt = opt->next;
    }

    last->next = new;
}

struct configuration_option*
get_configuration_option(
    struct configuration* configuration,
    const char* name)
{
    struct configuration_option* opt = configuration->options;

    while (opt != NULL)
    {
        if (strcmp(name, opt->name) == 0)
        {
            return opt;
        }
        opt = opt->next;
    }
    return NULL;
}

char*
configuration_option_to_string(
    struct configuration_option* option)
{
    char value[CONFIGURATION_VALUE_MAXLENGTH];

    switch (option->type)
    {
        case INTEGER:
        {
            int n = *((int*)option->value);
            snprintf(value, CONFIGURATION_VALUE_MAXLENGTH,
                "%d", n);
            break;
        }
        case FLOAT:
        {
            float n = *((float*)option->value);
            snprintf(value, CONFIGURATION_VALUE_MAXLENGTH,
                "%f", n);
            break;
        }
        case STRING:
            snprintf(value, CONFIGURATION_VALUE_MAXLENGTH,
                "%s", (char*) option->value);
            break;
        case IPV4:
            snprintf(value, CONFIGURATION_VALUE_MAXLENGTH,
                "%s", (char*) option->value);
            break;
    }

    int len = strlen(option->name) + 3 + strlen(value) + 1;
    char* option_string = malloc(len * sizeof(char));
    if (option_string == NULL)
    {
        fprintf(stderr,
            "Error allocating memory\n");
        return NULL;
    }
    snprintf(option_string, len, "%s = %s", option->name, value);
    return option_string;
}

struct configuration*
default_configuration()
{
    struct configuration* cfg = malloc(sizeof(struct configuration));
    if (cfg == NULL)
    {
        fprintf(stderr, "Error allocating memory for configuration\n");
        return NULL;
    }
    cfg->options = NULL;

    set_configuration_option(
        cfg,
        "bind_address",
        IPV4,
        "127.0.0.1");

    set_configuration_option(
        cfg,
        "port",
        INTEGER,
        & (int) {8987});

    return cfg;
}

void
read_configuration_file(struct configuration* cfg, char* path)
{
    FILE* f = fopen(path, "r");
    if (f == NULL)
    {
        fprintf(stderr, "Error opening configuration file\n");
        exit(EXIT_FAILURE);
    }

    char buffer[LINE_BUFFER_LENGTH];
    char* line;
    do {
        line = fgets(buffer, LINE_BUFFER_LENGTH, f);
        if (buffer[0] == '#')
        {
            continue;
        }

        char* name = strtok(buffer, "=");
        if (name == NULL)
        {
            continue;
        }
        char* value = strtok(NULL, "=");
        if (value == NULL)
        {
            continue;
        }

        // FIXME: name/value pointers never freed
        name = string_trim_whitespace(name);
        value = string_trim_whitespace(value);

        struct configuration_option* opt =
            get_configuration_option(cfg, name);
        if (opt == NULL)
        {
            printf("Opt not found\n");
            continue;
        }

        void* new_value;
        switch (opt->type)
        {
            case INTEGER:
            {
                new_value = malloc(sizeof(int));
                if (new_value == NULL)
                {
                    fprintf(stderr, "Out of memory!\n");
                    exit(EXIT_FAILURE);
                }
                int n = (int) strtol(value, NULL, 10);
                *((int*)new_value) = n;
                break;
            }
            case FLOAT:
            {
                new_value = malloc(sizeof(float));
                if (new_value == NULL)
                {
                    fprintf(stderr, "Out of memory!");
                    exit(EXIT_FAILURE);
                }
                float n = (float) strtod(value, NULL);
                *((float*)new_value) = n;
                break;
            }
            case STRING:
            case IPV4:
                new_value = strdup(value);
                break;
        }
        free(opt->value);
        set_configuration_option(cfg, name, opt->type, new_value);
    } while(line != NULL);

    fclose(f);
}
