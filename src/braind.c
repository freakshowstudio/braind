
#include "autoconf.h"

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "daemon.h"
#include "configuration.h"

static struct configuration*
get_configuration(int argc, char** argv)
{
    struct configuration* configuration = default_configuration();
    if (configuration == NULL)
    {
        return NULL;
    }

    char* path = malloc(
        (strlen(BRAIND_CFGFILE_PATH) + strlen("/braind.config") + 1)
        * sizeof(char));
    if (path == NULL)
    {
        free(configuration);
        return NULL;
    }

    strcat(path, BRAIND_CFGFILE_PATH);
    strcat(path, "/braind.config");
    read_configuration_file(configuration, path);
    return configuration;
}

int main(int argc, char** argv)
{
    struct configuration* configuration = get_configuration(argc, argv);
    if (configuration == NULL)
    {
        return EXIT_FAILURE;
    }
    return run_daemon(configuration);
}
